/*jshint esversion: 9 */
// process.env.NODE_ENV = 'production';
const path = require('path');
function resolve(dir) {
    return path.join(__dirname, dir);
}
module.exports = {
    // 选项...
    publicPath:'/',
    outputDir:'dist',
    assetsDir:'assets',
    productionSourceMap: false,  //不显示源码
    devServer: {
        host: '0.0.0.0',
        port: 8087,
        proxy: {
            '/api': {
                target: `http://101.35.169.80:8089/api`,
                // target: `http://192.168.0.61:8087/`,
                // target: `http://101.35.169.80:8087/`,
                changeOrigin: true,
                pathRewrite: {
                    ['^/api']: '',
                },
            },
        },
    },
    chainWebpack: (config) => {
        config.module
        .rule('svg')
        .exclude.add(resolve('src/icons'))
        .end();
        config.module
        .rule('icons')
        .test(/\.svg$/)
        .include.add(resolve('src/icons/')) //处理svg目录
        .end()
        .use('svg-sprite-loader')
        .loader('svg-sprite-loader')
        .options({
            symbolId: 'icon-[name]'
        });
    },
    pages: {
        index: {
            // page 的入口
            entry: 'src/main.js',
            // 模板来源
            template: 'public/index.html',
            // 在 dist/index.html 的输出
            filename: 'index.html',
            // 当使用 title 选项时，
            // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
            title: process.env.VUE_APP_TITLE,
            // 在这个页面中包含的块，默认情况下会包含
            // 提取出来的通用 chunk 和 vendor chunk。
            chunks: ['chunk-vendors', 'chunk-common', 'index'],
        },
    },
    configureWebpack: config => {
        config.externals = {
            'COS':'COS',
        };
    },
};