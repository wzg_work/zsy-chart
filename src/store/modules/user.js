//用户数据操作
import {localStorageX} from "storagex-js";
import VueCookies from 'vue-cookies';
import {Const} from "@/common/http";
const state = localStorageX(
    "userContainer",
    {
        token:"",
        user:{},
    }
);

const mutations = {
    setToken(_state,token){  //写入token  //同时写入cookie
        VueCookies.set(Const.TOKENKEY,token,{ expires: 30 });
        _state.token = token;
    },
    resetToken(_state){  //清空token
        _state.token = '';
        VueCookies.remove(Const.TOKENKEY);
    },
    setUser(_state,user){  //写入用户信息
        _state.user = user;
    },
    resetUser(_state){  //清空用户
        _state.user = {};
    },
}

const actions = {};

export default {
    namespaced: true,
    state,
    mutations,
    actions,
};

