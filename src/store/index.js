import Vue from 'vue';
import Vuex from 'vuex';

import user from './modules/user';
import _public from './modules/public';

Vue.use(Vuex);

export default new Vuex.Store({
    modules:{
        userModule:user,  //用户数据模块
        publicModule:_public,  //公共数据模块
    },
});
