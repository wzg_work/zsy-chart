/*jshint esversion: 9 */
/*
 color工具
 */

export function randomNum(minNum,maxNum){  //生成从minNum到maxNum的随机数
    switch(arguments.length){ 
        case 1: 
            return parseInt(Math.random()*minNum+1,10); 
        case 2: 
            return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10); 
        default: 
            return 0; 
    } 
}
export function randomColor() {  //获取随机颜色
    var rand = Math.floor(Math.random( ) * 0xFFFFFF).toString(16);
    if(rand.length == 6){
        return '#' + rand;
    }else{
        return randomColor();
    }
}
export function randomColors(length){  //获取随机颜色列表
    const colors = [];
    while(colors.length < length){
        const color = randomColor();
        if(colors.includes(color)) continue;
        colors.push(color);
    }
    return colors;
}
export function isLight(rgb){  //判断颜色是否为亮色
    let rgbValue = rgb.replace("rgb(", "").replace(")", "");
    let rgbValueList = rgbValue.split(',');
    return (rgbValueList[0] * 0.213 + rgbValueList[1] * 0.715 + rgbValueList[2] * 0.072)>(225/2);
}
export function colorRGBtoHex(color) {  //将rgb颜色转换成16进制
    if(!color) return color;
    var rgb = color.split(',');
    var r = parseInt(rgb[0].split('(')[1]);
    var g = parseInt(rgb[1]);
    var b = parseInt(rgb[2].split(')')[0]);
    var hex = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    return hex;
}