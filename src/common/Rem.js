/*jshint esversion: 9 */
/*
 css全局变量工具，一篇文字
 全局像素计算方法，设置基础的font-size，当宽度改变时按照倍数相应改变
 */
import {TaskControl} from './SimpleAnimation';
const taskServe = new TaskControl();

const baseSize = 16;  //默认 宽度1920时的基础尺寸 （px）

function scaleFilter(s){  //倍数过滤函数
    return s;
}

taskServe.add(()=>{
    let scale = document.documentElement.clientWidth / 1920;
    scale = scaleFilter(scale);
    let fontSize = baseSize * scale + 'px';
    if(document.documentElement.style.getPropertyValue('fontSize') != fontSize){
        document.documentElement.style.fontSize = fontSize;
    }
});