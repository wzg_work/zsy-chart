/*jshint esversion: 9 */
/*
 椭圆类
 依据方程 x^2/a^2 + y^2/b^2 = 1;
*/
export class Oval {
    /*
     椭圆中点，长轴点，短轴点
     */
    constructor(midPoint, aPoint, bPoint) {
        let aPoint_ = [aPoint[0] - midPoint[0], aPoint[1] - midPoint[1]];
        let bPoint_ = [bPoint[0] - midPoint[0], bPoint[1] - midPoint[1]];
        this.rotateRadian = Math.atan(bPoint_[0] / bPoint_[1]); //椭圆旋转弧度
        this.a = Math.sqrt(Math.pow(aPoint_[0], 2) + Math.pow(aPoint_[1], 2)); //长轴长
        this.b = Math.sqrt(Math.pow(bPoint_[0], 2) + Math.pow(bPoint_[1], 2)); //短轴长
        this.midPoint = midPoint;
    }
    /*
     根据角度获取点位
     */
    getPoint(angle) {
        let radian = (Math.PI / 180) * angle; //角度转弧度
        let x = this.a * Math.sin(radian) + this.midPoint[0];
        let y = this.b * Math.cos(radian) + this.midPoint[1];
        return this.pointRotate(...this.midPoint, x, y, this.rotateRadian);
    }
    /*
     点位旋转计算
     */
    pointRotate(cx, cy, x, y, radians) {
        var cos = Math.cos(radians),
            sin = Math.sin(radians),
            nx = (cos * (x - cx)) + (sin * (y - cy)) + cx,
            ny = (cos * (y - cy)) - (sin * (x - cx)) + cy;
        return [nx, ny];
    }
}

//测试
// (function(){
//     let oval = new Oval([0,0],[100,0],[0,100]);
//     var du=360,z=0,arr=[],timer;
//     var go=document.querySelector(".move");
//     for (var i = 0; i < du; i++) {
//         var divs=document.createElement("div");
//         let point = oval.getPoint(i);
//         let x = point[0] + 300;
//         let y = 300 - point[1];
//         divs.className="ab";
//         divs.style.cssText="left:"+(x)+"px;top:"+(y)+"px;"
//         document.body.appendChild(divs);
//     };
// })()