/*jshint esversion: 9 */
import {isProdoction} from './Tools';

const TOKENKEY = "ssid";  //上传token时对应的键名
const timeout = 70000;  //api请求超时时间
let baseApiURL;  //api原始链接
if(isProdoction()){  //如果是生产环境
    baseApiURL = '/api';
}else{
    baseApiURL = '/api';
}
export default{
    TOKENKEY,
    baseApiURL,
    timeout,
};