/*jshint esversion: 9 */
export function formatURLSearchParams(param = {}) { //格式化普通表单
    if(param instanceof URLSearchParams) return param;
    let _param = new URLSearchParams();
    for (let index in param) {
        if (param[index] === undefined) continue;
        _param.append(index, param[index]);
    }
    return _param;
}
export function formatFormData(param = {}) { //格式化带有文件的表单
    if(param instanceof FormData) return param;
    let _param = new FormData();
    for (let index in param) {
        if (param[index] === undefined) continue;
        _param.append(index, param[index]);
    }
    return _param;
}
export function formatUrlParams(param = {}) { //格式化get的链接式参数
    let str = "?";
    for (let index in param) {
        if (param[index] === undefined) continue;
        let _str;
        if (param[index] instanceof Object) {
            _str = encodeURIComponent(JSON.stringify(param[index])); //json序列化为字符串，并编码
        } else {
            _str = encodeURIComponent(param[index]);
        }
        str += index + '=' + _str + "&";
    }
    str = str.substr(0, str.length - 1);
    return str;
}
export function isProdoction(){  //是否是生产环境
    return process.env.NODE_ENV === "production";
}