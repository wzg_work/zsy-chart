/*jshint esversion: 9 */
import axios from "axios";
import Const from "./Const";

export const service = axios.create({  //可创建多个 axios实例
    baseURL: Const.baseApiURL, //设置公共的请求前缀
    timeout: Const.timeout, //超时终止请求
});

service.interceptors.request.use(
    config => {
        // config.headers.token = '';
        return config;
    },
    error => Promise.error(error),
);

service.interceptors.response.use(
    response => {
        const data = response.data;
        const sate = data.sate;
        switch (sate) {
            case 200:
                return data;
            case 201: //表示登陆失效
                return Promise.reject(data);
            default:
                return Promise.reject(data);
        }
    },
    () => { //数据请求发生错误
        return Promise.reject({
            msg: '网络发生波动，请稍后再试。',
        });
    },
);