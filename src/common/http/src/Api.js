/*jshint esversion: 9 */
import {
    service,
} from "./Request";
import {
    formatFormData,
} from "./Tools";

/*
 首页
 */
function getBaseData({params}={}){
    return service({
        url:'/avg',
        method:'get',
        params:params,
    });
}
function getReport({params}={}){
    return service({
        url:'/byNo',
        method:'get',
        params:params,
    });
}
function getWellNos({params}={}){
    return service({
        url:'/select1',
        method:'get',
        params:params,
    });
}
function getHorizons({params}={}){
    return service({
        url:'/select2',
        method:'get',
        params:params,
    });
}
function getReports({params}={}){
    return service({
        url:'/select3',
        method:'get',
        params:params,
    });
}
function uploadReport({data}={}){  //报告上传
    return service({
        url:'/report',
        method:'post',
        data:formatFormData(data),
    });
}
const reportNum = '/reportNum';  //报告数据
const mulreport = '/mulreport';  //多文件上传
/*
 日志管理
 */
const log = '/log/list';
/*
 报告管理
 */
const reportList = '/report/list';
const reportUpdate = '/report/update';
const reportDelete = '/report/delete';
/*
 地层水管理
 */
const waterList = '/water/list';  //数据；列表
const judgment = '/water/judgment';  //条件判识
const waterJudgment = '/water/judgmenttwo';  //条件判识
const waterSyneresis = '/water/waterSyneresis';  //凝析水
const waterStratum = '/water/waterstratum';  //地层水
const waterAcid = '/water/wateracid';  //酸化水
const waterSave = '/water/watersave';  //地层水保存条件判识
const productHorizon = '/water/productHorizon';  //产出层位判识判识
const chemical = '/water/chemical';  //基础数据化学相图
const waterOrigin = '/water/waterorigin';  //地层水成因判识
const featuresLon = '/water/featuresLon';  //特征离子剖面
const waterBaisc = '/water/baisc';  //判断地层水类型
const waterYinYang = '/water/yinyang';  //阴阳离子含量
/*
 天然气管理
 */
const steamList = '/steam/list';
const steamJudgment = '/steam/judgment';
const findEthane = '/steam/findEthane';  //组分特征-乙烷分布
const findByH2s = '/steam/findByH2s';  //单一来源天然气判识-H2S C02
const findByHEnrich = '/steam/findByHEnrich';  //单一来源天然气判识-天然气甲烷碳同位素δ13C1—富集系数（δ13C2- δ13C1）相关
const findByHydr = '/steam/findByHydr';  //单一来源天然气判识-重烃
const findCIsotope = '/steam/findCIsotope';  //组成特征-碳同位素
const findDry = '/steam/findDry';  //组成特征-干燥系数
const findHIsotope = '/steam/findHIsotope';  //组成特征-氢同位素
const findHydrocarbon = '/steam/findHydrocarbon';  //组成特征-重烃
const findOtherIsotope = '/steam/findOtherIsotope';  //组成特征-稀有同位素

export default{
    mulreport,
    reportNum,
    getBaseData,
    getReport,
    getWellNos,
    getHorizons,
    getReports,
    uploadReport,
    log,
    reportList,
    reportUpdate,
    reportDelete,
    judgment,
    waterJudgment,
    waterList,
    steamList,
    steamJudgment,
    waterSyneresis,
    waterStratum,
    waterAcid,
    waterSave,
    productHorizon,
    chemical,
    waterOrigin,
    featuresLon,
    waterBaisc,waterYinYang,
    findEthane,
    findByH2s,findByHEnrich,findByHydr,findCIsotope,findDry,findHIsotope,findHydrocarbon,findOtherIsotope,
};