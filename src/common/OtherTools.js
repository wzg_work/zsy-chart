/*jshint esversion: 9 */
//常用的其他工具
export function formatFileSize(fileSize) {  //格式化文件大小
    if(!fileSize) return fileSize;
    if(typeof fileSize !== 'number') return fileSize;
    var temp;
    if (fileSize < 1024) {
        return fileSize + 'B';
    } else if (fileSize < (1024*1024)) {
        temp = fileSize / 1024;
        temp = temp.toFixed(2);
        return temp + 'KB';
    } else if (fileSize < (1024*1024*1024)) {
        temp = fileSize / (1024*1024);
        temp = temp.toFixed(2);
        return temp + 'MB';
    } else {
        temp = fileSize / (1024*1024*1024);
        temp = temp.toFixed(2);
        return temp + 'GB';
    }
}
export function getFilePreviewState(fileName){  //判断文件是否可预览
    if(!fileName) return false;
    let types = fileName.split('.');
    let type = types[types.length - 1];
    return 'png|jpg|pdf'.indexOf(type) !== -1;
}
export function isvalidPhone(str) {  //验证电话号码
    const reg = /^1[0-9][0-9]\d{8}$/;
    return reg.test(str);
}
export function getFileType(fileName){  //获取文件类型
    if(!fileName) return fileName;
    let types = fileName.split('.');
    return types[types.length - 1];
}
export function randomString(e) {  //随机获取相应位数的字符串
    e = e || 32;
    let t = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678",
    a = t.length,
    n = "";
    for (let i = 0; i < e; i++) n += t.charAt(Math.floor(Math.random() * a));
    return n;
}
export function highlightFilter(text,hlText){  //高亮文本过滤器
    if(!text || !hlText) return text;
    return text.replace(new RegExp(hlText,'g'),`<span>${hlText}</span>`);
}
export function doubleClick(callBack) { //双击事件
    let time = 0;
    return function () {
        let now = new Date().getTime();
        if ((now - time) < 300) { //表示是双击事件
            callBack.call(this, ...arguments);
        }
        time = now;
    };
}
export function computedRem(){  //计算rem,单位px
    return parseInt(document.documentElement.style.fontSize);
}
export function isProduction(){  //是否是生产环境
    return process.env.NODE_ENV == 'production';
}
export function isEmptyObject(target){  //是空对象
    if(!target) return true;
    return Object.keys(target).length === 0;
}
export function randomNum(minNum,maxNum){  //生成从minNum到maxNum的随机数
    switch(arguments.length){ 
        case 1: 
            return parseInt(Math.random()*minNum+1,10); 
        case 2: 
            return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10); 
        default: 
            return 0; 
    } 
}
export function randomEchartSymbol(){  //随机echart图形
    let symbols = ['circle','rect','roundRect','triangle','diamond'];
    return symbols[randomNum(0,symbols.length)];
}
export function scrollDirect({target,fn,upFn,downFn,leftFn,rightFn}={}){  //判断滚动条滚动方向
    let newY = 0;
    let oldY = 0;
    let newX = 0;
    let oldX = 0;
    target.addEventListener("scroll",function(){
        newY = target==window?this.scrollY:this.scrollTop;
        newX = target==window?this.scrollX:this.scrollLeft;
        fn && fn({
            newX,newY,oldX,oldY,
        });
        if(newY>oldY){
            downFn && downFn({newY,oldY});
        }else{
            upFn && upFn({newY,oldY});
        }
        if(newX>oldX){
            rightFn && rightFn({newX,oldX});
        }else{
            leftFn && leftFn({newX,oldX});
        }
        oldY = newY;
        oldX = newX;
    });
}
export function numberLimit({value,min,max}={}){  //数字限制
    return Math.min(Math.max(value,min),max);
}