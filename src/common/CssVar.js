/*jshint esversion: 9 */
/*
 css全局变量工具，一篇文字
 */
import {TaskControl} from './SimpleAnimation';
const taskServe = new TaskControl();

const clientHeight = '--clientHeight'; //屏幕可用区域高度

taskServe.add(()=>{
    let clientHeight_ = `min(${document.documentElement.clientHeight}px, 100vh)`;
    if (document.documentElement.style.getPropertyValue(clientHeight) != clientHeight_) {
        document.documentElement.style.setProperty(
            clientHeight,
            clientHeight_,
        ); //可见区域高度  css全局变量
    }
});