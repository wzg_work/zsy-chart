/*jshint esversion: 9 */
import Vue from 'vue';
import VueRouter from 'vue-router';

import notfount from "@/views/notFount.vue";
import operate from "@/layout/operate";
import waterIdent from '@/views/waterIdent';
import log from '@/views/log';
import report from '@/views/report';
import baseData from '@/views/baseData';
import gas from '@/views/gas';

Vue.use(VueRouter);
const VueRouterPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(to) { //修改错误提示
    return VueRouterPush.call(this, to).catch((err) => err);
};

const routes = [
    {
        path: '*', //404页面
        name: 'Notfount',
        component: notfount,
        meta: {
            title: '没找到该页面',
            keepAlive: true,
        },
    },
    {
        path: '/',
        name: 'Operate',  //操作面板界面
        component:operate,
        meta:{
            keepAlive:true,
            title: '操作面板',
        },
        children:[
            { path: '/', redirect: '/basedata' },
            {
                path: '/waterident',
                name: 'WaterIdent',
                component: waterIdent,
                meta: {
                    title:'地层水判识',
                    keepAlive:false,
                },
            },
            {
                path: '/report',
                name: 'Report',
                component: report,
                meta: {
                    title:'报告管理',
                    keepAlive:true,
                },
            },
            {
                path: '/basedata',
                name: 'BaseData',
                component: baseData,
                meta: {
                    title:'基础数据管理',
                    keepAlive:false,
                },
            },
            {
                path: '/gas',
                name: 'Gas',
                component: gas,
                meta: {
                    title:'天然气',
                    keepAlive:false,
                },
            },
            {
                path: '/log',
                name: 'Log',
                component: log,
                meta: {
                    title:'日志管理',
                    keepAlive:true,
                },
            },
        ],
    },
];

const router = new VueRouter({
    mode:'history',
    routes,
});

export default router;