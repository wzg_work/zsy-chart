/*jshint esversion: 9 */
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';

// 引入ElementUI
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);
//引入图表库
import * as echarts from 'echarts';
Vue.prototype.$echarts = echarts;
//引入全局css
import "@/style/PublicStyle.scss";
import "@/style/CssVar.scss";
import "@/style/ElementUi.scss";
import '@/common/CssVar';  //引入css变量计算方法
import '@/common/Rem';  //引入rem计算方法
//引入Api,以及请求公共变量,axios实例
import {
    Api,
    Const,
    service,
} from "@/common/http";
Vue.prototype.$Api = Api;
Vue.prototype.$Const = Const;
Vue.prototype.$Service = service;

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');